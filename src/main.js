import { createApp } from 'vue'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueAnimateOnScroll from 'vue3-animate-onscroll';

import router from './router'
import BButton from './components/B-Button.vue'
library.add(fas, far, fab);
const app = createApp(App);

app.component('v-icon', FontAwesomeIcon);
app.use(router);
app.use(VueAnimateOnScroll);
app.component('BButton', BButton);

global.app = app.mount('#app');
global.refs = global.app.$refs;
