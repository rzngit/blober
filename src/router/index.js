import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Blober - Bendruomenė skirta dalintis kūryba',
    component: () => import('@/views/HomeView.vue'),
    meta: {
      title: 'Blober - Bendruomenė skirta dalintis kūryba',
      metaTags: [
        {
          name: 'description',
          content: 'Viena iš geriausių bendruomenių skirta dalintis bei atrasti kūrybos. Įgauti daugiau patirties ir naudoti savo talentą kuri yra svarbu.'
        },
        {
          property: 'keywords',
          content: 'blober, naryste, dizainas, kursai, pamokos, kuryba, web dizainas, ui, ux, iliustracijos, animacijos, fotografijos'
        }
      ]
    },
  },
  {
    path: '/ismok',
    name: 'Blober - Išmok dizaino',
    component: () => import('@/views/IsmokView.vue'),
    meta: {
      title: 'Blober - Išmok dizaino',
      metaTags: [
        {
          name: 'description',
          content: 'Mūsų 10 savaičių trunkantys kursai gali būti puikiai pritaikyti pagal Jūsų laiką. Susipažinę su profesionaliais dizaineriais įgysite platesnį supratimą apie dizainerio karjerą, pagerinsite savo įgūdžius kurdami kas jums patinka.'
        },
        {
          property: 'keywords',
          content: 'blober, naryste, dizainas, kursai, pamokos, kuryba, web dizainas, ui, ux, iliustracijos, animacijos, fotografijos'
        }
      ]
    },
  },
  {
    path: '/parduotuve',
    name: 'Blober - Parduotuvė',
    component: () => import('@/views/ParduotuveView.vue'),
    meta: {
      title: 'Blober - Parduotuvė',
      metaTags: [
        {
          name: 'description',
          content: 'Pradžiuginkite save ar paspartinkite savo darbą naudodami kitų sukurtus projektus!'
        },
        {
          property: 'keywords',
          content: 'blober, naryste, dizainas, kursai, pamokos, kuryba, web dizainas, ui, ux, iliustracijos, animacijos, fotografijos'
        }
      ]
    },
  },
  {
    path: '/naryste',
    name: 'Blober- Narystė',
    component: () => import('@/views/NarysteView.vue'),
    meta: {
      title: 'Blober - Narystė',
      metaTags: [
        {
          name: 'description',
          content: 'Išplėsk savo galimybes su Blober Naryste.'
        },
        {
          property: 'keywords',
          content: 'blober, naryste, dizainas, kursai, pamokos, kuryba, web dizainas, ui, ux, iliustracijos, animacijos, fotografijos'
        }
      ]
    },
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes: routes,
  scrollBehavior() {
    return { top: 0 }
  },
})

router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // e.g., if we have `/some/deep/nested/route` and `/some`, `/deep`, and `/nested` have titles,
  // `/nested`'s will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) {
    document.title = nearestWithTitle.meta.title;
  } else if(previousNearestWithMeta) {
    document.title = previousNearestWithMeta.meta.title;
  }

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => document.head.appendChild(tag));

  next();
});

export default router
