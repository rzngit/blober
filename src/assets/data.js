export const data = [
    {
        "Pav": "My Bed Revival",
        "Aut": "Walter Boone",
        "Views":200,
        "Likes":25,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 4,
        "URL":"https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832__480.jpg"
    },
    {
        "Pav": "Spiffing Blond Day",
        "Aut": "Bradley Walters",
        "Views":120,
        "Likes":2425,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 6,
        "URL":"https://images.unsplash.com/photo-1491466424936-e304919aada7?ixlib=rb-4.0.3&w=1080&fit=max&q=80&fm=jpg&crop=entropy&cs=tinysrgb"
    },
    {
        "Pav": "No Stick",
        "Aut": "Romeo Gill",
        "Views":21230,
        "Likes":24215,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 56,
        "URL":"https://cdn.pixabay.com/photo/2018/08/14/13/23/ocean-3605547__480.jpg"
    },
    {
        "Pav": "The Crazy Arms",
        "Aut": "Angelica Howell",
        "Views":2120,
        "Likes":235,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 45,
        "URL":"https://www.teahub.io/photos/full/1-14054_aesthetic-desktop-wallpaper-aesthetic-pc-wallpapers-hd.jpg"
    },
    {
        "Pav": "Kazkas",
        "Aut": "Arnold Bryant",
        "Views":4320,
        "Likes":235,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 1,
        "URL":"https://1.bp.blogspot.com/-sqz6OiG20UI/X0WO9CGxO-I/AAAAAAAAaLs/yfUXlx2SaaAzpadTd7DucJyHIuQH2hrFACLcBGAsYHQ/w640-h360/MInimalist-landscape-wallpaper-night-moon-desktop-hd.png"
    },
    {
        "Pav": "Three Inch Arms",
        "Aut": "Aliya Armstrong",
        "Views":2120,
        "Likes":25,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 5,
        "URL":"https://c4.wallpaperflare.com/wallpaper/448/174/357/neon-4k-hd-best-for-desktop-wallpaper-preview.jpg"
    },
    {
        "Pav": "Arms for the Sprites",
        "Aut": "Elijah Combs",
        "Views":2012,
        "Likes":235,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 12,
        "URL":"https://www.pixelstalk.net/wp-content/uploads/images2/Free-Download-Computer-Wallpaper-HD-for-Desktop.jpg"
    },
    {
        "Pav": "Stick Attack",
        "Aut": "Jaxon Bush",
        "Views":510,
        "Likes":456,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 6,
        "URL":"https://wallpapers.com/images/hd/for-your-desktop-43-top-quality-hd-pc-wallpaper-bscb-wpbg-9roy8i4xos0cju3e.jpg"
    },
    {
        "Pav": "Deaf Badgers",
        "Aut": "Alannah Sparks",
        "Views":12,
        "Likes":56,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 45,
        "URL":"https://media.idownloadblog.com/wp-content/uploads/2022/08/Aqueux-idownloadblog-mockup-1500x1000.jpg"
    },
    {
        "Pav": "Spinning Dolls",
        "Aut": "Donovan Smith",
        "Views":34,
        "Likes":234,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 68,
        "URL":"https://w0.peakpx.com/wallpaper/112/562/HD-wallpaper-lord-shiva-artwork-others.jpg"
    },
    {
        "Pav": "Beyond My Bed",
        "Aut": "Gwen Wagner",
        "Views":623,
        "Likes":724,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 62,
        "URL":"https://i.pinimg.com/736x/96/14/09/9614099b93b44e2ca5dafea86206ceb6.jpg"
    },
    {
        "Pav": "Hard Game",
        "Aut": "Gwen Wagner",
        "Views":63,
        "Likes":4,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 61,
        "URL":"https://media.idownloadblog.com/wp-content/uploads/2022/03/euphoria-wallpapers-by-its_shevi-idownloadblog-mock-up.jpeg"
    },
    {
        "Pav": "Thorn in the Ring",
        "Aut": "Gwen Wagner",
        "Views":6,
        "Likes":74,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 12,
        "URL":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNgrd2kWs5tb7Sk78WaSa2UG7_4ksfJD_3sQ&usqp=CAU"
    },
    {
        "Pav": "The Ice of the Dream",
        "Aut": "Gwen Wagner",
        "Views":3,
        "Likes":34,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 62,
        "URL":"https://c4.wallpaperflare.com/wallpaper/974/565/254/windows-11-windows-10-minimalism-hd-wallpaper-preview.jpg"
    },
    {
        "Pav": "The Thoughts's Future",
        "Aut": "Gwen Wagner",
        "Views":6,
        "Likes":24,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 78,
        "URL":"https://c4.wallpaperflare.com/wallpaper/376/330/356/digital-art-artwork-illustration-concept-art-environment-hd-wallpaper-preview.jpg"
    },
    {
        "Pav": "Hustler of Predator",
        "Aut": "Gwen Wagner",
        "Views":33,
        "Likes":34,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 1,
        "URL":"https://i.pinimg.com/736x/02/dd/09/02dd093bd8def377c98558c38df72531.jpg"
    },
    {
        "Pav": "The Missing Mists",
        "Aut": "Gwen Wagner",
        "Views":3,
        "Likes":84,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina":5,
        "URL":"https://c4.wallpaperflare.com/wallpaper/39/346/426/digital-art-men-city-futuristic-night-hd-wallpaper-thumb.jpg"
    },
    {
        "Pav": "Cracked Birth",
        "Aut": "Gwen Wagner",
        "Views":51,
        "Likes":421,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 65,
        "URL":"https://www.lifewire.com/thmb/_cH2YBd-iyPP1cqqj2WWXiCWudg=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/hdwallpapersnet-free-wallpaper-5919d3ca3df78cf5fa49bda3.jpg"
    },
    {
        "Pav": "Game in the Moon",
        "Aut": "Gwen Wagner",
        "Views":345,
        "Likes":234,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 64,
        "URL":"https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832__480.jpg"
    },
    {
        "Pav": "The Hustler of the Danger",
        "Aut": "Gwen Wagner",
        "Views":234,
        "Likes":12,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 27,
        "URL":"https://www.freewalldownload.com/download/lake-attractive-rivers-wallpaper-free-download/"
    },
    {
        "Pav": "The Man's Female",
        "Aut": "Gwen Wagner",
        "Views":3232,
        "Likes":52,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 64,
        "URL":"https://mobimg.b-cdn.net/v3/fetch/ae/ae55d2a571874c9a44141b09f1711960.jpeg"
    },
    {
        "Pav": "Serpent of Gift",
        "Aut": "Gwen Wagner",
        "Views":23,
        "Likes":66,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 63,
        "URL":"https://www.lifewire.com/thmb/o28h74cOGYYKBikaHHu2_YpcLSs=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/tropical-beach-wallpaper-beach-backgrounds-587fbb765f9b584db3241860.jpg"
    },
    {
        "Pav": "The Ragged Souls",
        "Aut": "Gwen Wagner",
        "Views":12,
        "Likes":724,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 163,
        "URL":"https://c4.wallpaperflare.com/wallpaper/108/140/869/digital-digital-art-artwork-fantasy-art-drawing-hd-wallpaper-preview.jpg"
    },
    {
        "Pav": "Blue Hunter",
        "Aut": "Gwen Wagner",
        "Views":2,
        "Likes":2,
        "Ikelta": randomDate(new Date(2012, 0, 1), new Date()),
        "Kaina": 2,
        "URL":"https://c4.wallpaperflare.com/wallpaper/246/739/689/digital-digital-art-artwork-illustration-abstract-hd-wallpaper-preview.jpg"
    }
]

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }